 

Úkoly 

Vytvořit základní grid stránky za pomoci knihovny Vuetify (sidebar, navbar, topbar, footer) 
Za pomoci routeru vytvořit 2 stránky a zobrazit je v sidebaru 
Na hlavní stránce udělat formulář s validací za pomoci Vuetify a axios odesíláním na REST Api (nemusí být nikam napojeno) 
Na druhé stránce vložit tlačítko na přepínání jazyku a s využitím knihovny i18n měnit po kliknutí v navbaru větu v angličtině a češtině (text si určete sám) 