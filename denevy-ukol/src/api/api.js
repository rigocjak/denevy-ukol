import axios from 'axios';

const addUser = (user) => (
    new Promise((resolve, reject) => {
        axios.post('/user', user).then((response) => {
            resolve(response);
        }).catch((error) => reject(error));
    })
);

export default addUser;
