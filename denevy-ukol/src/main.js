import Vue from 'vue';
import VueI18n from 'vue-i18n';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import messages from './i18n/messages';

Vue.config.productionTip = false;
Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: 'en',
    messages,
});

new Vue({
    router,
    i18n,
    vuetify,
    render: (h) => h(App),
}).$mount('#app');
