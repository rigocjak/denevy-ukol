const messages = {
    en: {
        message: {
            about: 'About us',
            home: 'Home',
        },
        form: {
            name: 'Name',
            validationNameText: 'Name is required',
            validationNameLength: 'Name must be less than 10 characters',
            password: 'Password',
            validationPasswordText: 'Password is required',
            validationPasswordLength: 'Password must be less than 32 characters',
            email: 'E-mail',
            validationEmailText: 'E-mail is required',
            validationEmailForm: 'E-mail must be valid',
        },
        buttons: {
            submit: 'Submit',
            clear: 'Clear',
        },
    },
    cz: {
        message: {
            about: 'O nás',
            home: 'Domov',
        },
        form: {
            name: 'Jméno',
            validationNameText: 'Jméno je povinné',
            validationNameLength: 'Jméno musí být méně než 10 znaků',
            password: 'Heslo',
            validationPasswordText: 'Heslo je povinné',
            validationPasswordLength: 'Heslo musí být méně než 32 znaků',
            email: 'E-mail',
            validationEmailText: 'E-mail je povinný',
            validationEmailForm: 'E-mail musí být validní',
        },
        buttons: {
            submit: 'Potvrdit',
            clear: 'Zmazat',
        },
    },
};

export default messages;
